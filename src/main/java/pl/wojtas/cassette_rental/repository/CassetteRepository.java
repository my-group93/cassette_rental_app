package pl.wojtas.cassette_rental.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.wojtas.cassette_rental.model.Cassette;

@Repository
public interface CassetteRepository extends JpaRepository<Cassette, Long> {


}
