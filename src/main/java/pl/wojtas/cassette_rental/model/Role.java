package pl.wojtas.cassette_rental.model;

public enum Role {
    ROLE_USER, ROLE_ADMIN;
}
